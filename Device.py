class Device:

    def __init__(self, device_id, ip_address, patient_name, state):
        self.state = state
        self.device_id = device_id
        self.ip_address = ip_address
        self.patient_name = patient_name

    def set_state(self, state):
        self.state = state
