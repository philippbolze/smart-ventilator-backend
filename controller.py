from flask import Flask, render_template, request
import Device

app = Flask(__name__)

devices = []
latest_device_id = 0


def build_new_device(ip, patient, state):
    global latest_device_id
    latest_device_id = latest_device_id +1
    device_id = latest_device_id
    return Device.Device(device_id, ip, patient, state)


@app.route("/update")
def update_device():
    device_id = int(request.args.get('id'))
    state = request.args.get('state')
    for device in devices:
        if device.device_id == device_id:
            device.state = state
            return "success"
            break
    return "device not found"


@app.route("/register")
def register_device():
    ip_address = request.args.get('ip')
    patient_name = request.args.get('patient')
    state = request.args.get('state')
    device = build_new_device(ip_address, patient_name, state)
    devices.append(device)
    return str(device.device_id)


@app.route('/')
def overview():
    return render_template('overview.html', device_Models=devices)


if __name__ == '__main__':
    devices.append(build_new_device("123.123.123.123", "John Doe", "ok"))
    devices.append(build_new_device("123.123.123.123", "Anonymous", ""))
    devices.append(build_new_device("123.123.123.123", "Mary Doe", "warning"))
    devices.append(build_new_device("123.123.123.123", "Max Mustermann", "error"))
    devices.append(build_new_device("123.123.123.123", "Ronny Verdichter", "invalid state"))
    app.run(debug=True)
